source /opt/boxen/env.sh

# Git branch in prompt.

parse_git_branch() {

    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'

}

export PS1="\u@\H:\w$ \W\[\033[32m\]\$(parse_git_branch)\[\033[00m\] $ "

[ -f ~/.fzf.bash ] && source ~/.fzf.bash


export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm

export NVM_DIR=~/.nvm
source $(brew --prefix nvm)/nvm.sh

alias bx="bundle exec"

alias addsshkeys='ssh-add -K ~/.ssh/id_rsa && ssh-add -K ~/.aws/identity-production-1-us-east-1 && ssh-add -K ~/.aws/identity-staging-2-ap-southeast-2.pem'

git() {
    if [ "$1" = "add" -o "$1" = "stage" ]; then
        if [ "$2" = "." ]; then
            printf "'git %s .' is currently disabled by your Git wrapper.\n" "$1";
        else
            command git "$@";
        fi
    else
        command git "$@";
    fi;
}

git_current_branch() {
  git rev-parse --abbrev-ref HEAD
}
alias gpull='git pull origin `git_current_branch`'
alias gpush='git push origin `git_current_branch`'
alias gcp='git cherry-pick'
alias gs='git status'
alias rg='rg --hidden'


export PATH="$HOME/.rbenv/bin:$PATH"
eval "$(rbenv init -)"
