imap <S-CR> <CR><CR>end<Esc>-cc
set tabstop=2
set shiftwidth=2
set expandtab
set noswapfile
set number
set paste
set hlsearch
colorscheme molokai
set ignorecase
set rtp+=~/.fzf
let NERDTreeShowHidden=1
let NERDTreeIgnore=['\.DS_Store$']
nnoremap <C-p> :FZF<CR>
nnoremap <C-s> :w<CR>
nnoremap <C-g> :vertical resize +10<CR>
nnoremap <C-f> :vertical resize -10<CR>
nnoremap <C-d> :resize -10<CR>
nnoremap <C-e> :resize +10<CR>
nnoremap <C-t> :NERDTree<CR>
let g:go_version_warning = 0

let g:NERDTreeDirArrowExpandable="+"
let g:NERDTreeDirArrowCollapsible="~"
let $FZF_DEFAULT_COMMAND='find .'

set noeb vb t_vb=

autocmd VimEnter * NERDTree

nnoremap <silent> <Leader>x :call fzf#run({
\   'down': '40%',
\   'sink': 'botright split' })<CR>

" Open files in vertical horizontal split
nnoremap <silent> <Leader>v :call fzf#run({
\   'right': winwidth('.') / 2,
\   'sink':  'vertical botright split' })<CR>


if exists('+colorcolumn')
  set colorcolumn=80
else
  au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>80v.\+', -1)
endif

execute pathogen#infect()

syntax on

filetype plugin indent on
set backspace=indent,eol,start

call plug#begin('~/.vim/plugged')
  Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
  Plug 'ntpeters/vim-better-whitespace'
  Plug 'elixir-lang/vim-elixir'
  Plug 'scrooloose/nerdtree'
  Plug 'scrooloose/nerdcommenter'
  Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
call plug#end()

execute pathogen#infect()
