#!/bin/sh

#ACK
#sudo apt-get update
#sudo apt-get install ack-grep
#sudo dpkg-divert --local --divert /usr/bin/ack --rename --add /usr/bin/ack-grep


#extracting out file
mv .vim ~/
mv .bash_profile ~/
mv .vimrc ~/

#installing Pathogen.vim
mkdir -p ~/.vim/autoload ~/.vim/bundle && \
curl -LSso ~/.vim/autoload/pathogen.vim https://tpo.pe/pathogen.vim

#installing vim-plug
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim


############ Insert Plugs

printf "
execute pathogen#infect()\n
syntax on\n
filetype plugin indent on\n

call plug#begin('~/.vim/plugged')
  Plug 'ntpeters/vim-better-whitespace'
  Plug 'elixir-lang/vim-elixir'
  Plug 'scrooloose/nerdtree'
  Plug 'scrooloose/nerdcommenter'
  Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
call plug#end()
" >> ~/.vimrc


printf "
 export VISUAL=vim
 export EDITOR="$VISUAL"
  # Git branch in prompt.

  parse_git_branch() {

      git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \\(.*\\)/ (\\1)/'

  }
" >> ~/.bashrc

#VIM commands
printf "
Run these in vim...\n

Reload .vimrc and :PlugInstall to install plugins.

"
